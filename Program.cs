﻿using System;

namespace task
{
    class Program
    {
        static void Main(string[] args)
        {
            Sum();
            Area_rectangle();
            Area_circle();
            Even_odd();
            Sum_number();

            void Sum()
            {
                Console.WriteLine("Рассчет суммы чисел");
                int a, b, sum;
                Console.WriteLine("Введите первое число ");
                a = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Введите второе число ");
                b = Convert.ToInt32(Console.ReadLine());
                sum = a + b;
                Console.WriteLine("Сумма чисел " + sum);
            }

            void Area_rectangle()
            {
                Console.WriteLine("Расчет площади прямоугольника");
                int width, height, area;
                Console.WriteLine("Введите ширину прямоугольника ");
                width = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Введите высоту прямоугольника ");
                height = Convert.ToInt32(Console.ReadLine());

                area = width * height;

                Console.WriteLine("Площаль прямоугольника равна " + area);
            }

            void Area_circle()
            {
                Console.WriteLine("Расчет площади круга");
                double radius, area;
                Console.WriteLine("Введите радиус круга ");
                radius = Convert.ToDouble(Console.ReadLine());

                area = 3.14 * radius * radius;

                Console.WriteLine("Площаль круга равна " + area);
            }

            void Even_odd()
            {
                Console.WriteLine("Определение четности или нечетности числа");
                int number;
                Console.WriteLine("Введите число ");
                number = Convert.ToInt32(Console.ReadLine());

                if (number % 2 == 0)
                {
                    Console.WriteLine($"Число {number} четное ");
                }
                else Console.WriteLine($"Число {number} не четное ");

            }

            void Sum_number()
            {
                Console.WriteLine("Расчет суммы элементов массива");
                int[] numbers;
                int i, sum = 0;

                Console.WriteLine("Введите количество элементов масива");
                i = Convert.ToInt32(Console.ReadLine());

                numbers = new int[i];
                for (i = 0; i < numbers.Length; i++)
                {
                    numbers[i] = numbers[i] + i;
                    Console.WriteLine(numbers[i]);

                    sum = sum + numbers[i];


                }
                Console.WriteLine($"Сумма элементов {sum}");
            }
        }
    }
}




